from datetime import datetime
from session import db

class User(db.Model):
    __fillable__ = ['username', 'password']
    __hidden__ = ['created_at', 'updated_at', 'password']

    @property
    def posts(self):
        return self.has_many('posts')

    @property
    def roles(self):
        return self.belongs_to_many('tags')

    def __repr__(self):
        return '<User %r>' % self.username


class Post(db.Model):
    __fillable__ = ['title', 'content', 'user_id', 'tag_id', 'published_at']
    __hidden__ = ['created_at', 'updated_at', 'user_id', 'tag_id']

    @property
    def user(self):
        return self.belongs_to('users')

    @property
    def tag(self):
        return self.belongs_to('tags')

    def test(self):
        return self.user.username

    def scope_published(self, query):
        return query.where('published_at', '<=', datetime.now())

    def scope_unpublished(self, query):
        return query.where('published_at', '>', datetime.now())

    def scope_has_tag(self, query, tag):
        # posts = Post.where_has('tag', lambda q: q.where('name', 'AI')).get()

        return query.where_has('tag', lambda q: q.where('name', tag))

    def scope_has_user(self, query, username):
        return query.where_has('user', lambda q: q.where('username', username))

    def __repr__(self):
        return '<Post title %r>' % self.title


class Tag(db.Model):
    __fillable__ = ['name']
    __hidden__ = ['created_at', 'updated_at', 'pivot']

    @property
    def posts(self):
        return self.has_many('posts')

    @property
    def users(self):
        return self.belongs_to_many('users')
