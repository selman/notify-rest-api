#Obi Wan Kenotify

### POST /login

######Request:
```javascript
{
   "username" : "doga",
   "passowrd" : "doga"
}
```

######Response:
```javascript
{

    "Profile":{
        "id":1,
        "password":"doga",
        "roles":[
            {
                "id":1,
                "name":"AI"
            },
            {
                "id":3,
                "name":"admin"
            }
        ],
        "username":"doga"
    }

}
```


### POST /posts/create

######Request:
```javascript
{

    "username":"doga",
    "title":"Test Content",
    "tag":"AI",
    "content":"Test",
    "published_at":"26.08.2015 15:00"

}
```

######Response:
```javascript
{
    "message": "Post created successfully",
    "status_code": 201
}
```

### PUT /posts/< integer:id >

######Request:
```javascript
{

    "username":"doga",
    "title":"Test Content Updated",
    "tag":"AI",
    "content":"Test Updated",
    "published_at":"28.08.2015 15:00"

}
```

######Response:
```javascript
{
    "message": "Post updated successfully",
    "status_code": 200
}
```

### DELETE /posts/< integer:id >

######Response:
```javascript
{
    "message": "Post deleted successfully",
    "status_code": 200
}
```
### GET /posts/tag/< string:tagname >

######Response:
```javascript
{

    "Posts":[
        {
            "content":"Test",
            "id":1,
            "published_at":26.08.2015 15:00,
            "tag":{
                "id":1,
                "name":"AI"
            },
            "title":"test",
            "user":{
                "id":1,
                "username":"doga"
            }
        },
        {
            "content":"Test Content",
            "id":2,
            "published_at":26.08.2015 15:00,
            "tag":{
                "id":1,
                "name":"AI"
            },
            "title":"Test post",
            "user":{
                "id":2,
                "username":"admin"
            }
        }
    ]

}
```
### GET /posts/user< string:username >

######Response:
```javascript
{

    "Posts":[
        {
            "content":"Test",
            "id":1,
            "published_at":26.08.2015 15:00,
            "tag":{
                "id":1,
                "name":"AI"
            },
            "title":"test",
            "user":{
                "id":1,
                "username":"doga"
            }
        },
        {
            "content":"Test Content",
            "id":2,
            "published_at":26.08.2015 15:00,
            "tag":{
                "id":1,
                "name":"AI"
            },
            "title":"Test post",
            "user":{
                "id":1,
                "username":"doga"
            }
        }
    ]

}
```

### GET /tags
```javascript
{
  "Roles": [
    {
      "id": 1,
      "name": "AI"
    },
    {
      "id": 2,
      "name": "admin"
    },
    {
      "id": 4,
      "name": "test"
    },
    {
      "id": 5,
      "name": "embedded"
    }
  ]
}
```

### GET /posts/tag/< string:tagname >/published
	List published posts by tag

### GET /posts/tag/< string:tagname >/unpublished
	List unpublished posts by tag

### GET /posts/user/< string:username >/published
	List published posts by username

### GET /posts/user/< string:username >/unpublished
	List unpublished posts by username
