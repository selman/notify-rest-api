# -*- coding: utf-8 -*-
from flask import Flask, session, redirect, url_for, escape, request, abort
import os
from flask.json import jsonify
from flask_orator import Orator
from datetime import datetime
from reverse_proxy import ReverseProxied

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = True
app.config['ORATOR_DATABASES'] = {
    'development': {
        'driver': 'sqlite',
        'database': basedir + '/orator-models-branch.db'
    }
}
app.wsgi_app = ReverseProxied(app.wsgi_app)
db = Orator(app)

import models

@app.errorhandler(400)
def bad_request(error):
    response = jsonify({'status_code': 400,
                        'message': error.description})
    response.status_code = 400
    return response


@app.errorhandler(401)
def bad_request(error):
    response = jsonify({'status_code': 401,
                        'message': error.description})
    response.status_code = 401
    return response


@app.errorhandler(403)
def forbidden(error):
    response = jsonify({'status_code': 403,
                        'message': error.description})
    response.status_code = 403
    return response


@app.errorhandler(404)
def not_found(error):
    response = jsonify({'status_code': 404,
                        'message': error.description})
    response.status_code = 404
    return response


@app.errorhandler(409)
def conflict(error):
    response = jsonify({'status_code': 409,
                        'message': error.description})
    response.status_code = 409
    return response


@app.route('/q')
def q():
    data = models.Post.where('id', 1).first().test()
    return jsonify(results=data)


@app.route('/')
def index():
    if 'username' in session:
        # return
        return "Logged in as %s " % escape(session['username']) + escape(str(session)[1:-1])
    return 'You are not logged in'


def validate_date(date):
    try:
        datetime.strptime(date, "%Y-%m-%d %H:%M")
        return True
    except ValueError:
        return False


@app.route('/tags')
def tags():
    return jsonify(Roles=models.Tag.all().to_dict())
# {"username":"aa", "title":"bbb", "tag":"dskalda", "content":"dskaldaks", "published_at":"dsjkaldsa"}
@app.route('/create', methods=['POST'])
def create():
    #   session control, role control, sessiondan userı al, gelenle kontrol et
    expected_key_list = ['username',
                         'title',
                         'tag',
                         'content',
                         'published_at']
    post = escape_post(request.get_json(force=True))



    if ('username' in session) and (session['username'] == post['username']):

        if sorted(expected_key_list) != sorted(post.keys()) or validate_date(post['published_at']) is False:
            abort(400, 'Invalid request.')

        tag = models.Tag.where('name', post['tag']).first()
        if tag is None:
            abort(404, 'Tag can\'t found')

        # validate if user have this role
        if check_role(session['roles'], tag) is not True:
            abort(401, 'You need to login to access this tag.')


        user = models.User.where('id', session['user_id']).first()
        published_at = post['published_at']  # parser.parse()

        new_post = user.posts().create(
            title=post['title'],
            content=post['content'],
            tag_id=tag.id,
            published_at=published_at
        )


        if new_post is not None:
            response = jsonify({'message': 'Post created successfully',
                                'status_code': 201})
            response.status_code = 201
            return response

    else:
        return abort(401, 'You need to login to create a post.')


def escape_post(post):
    post['username'] = escape(post['username'])
    post['title'] = escape(post['title'])
    post['tag'] = escape(post['tag'])
    post['content'] = escape(post['content'])

    return post
# for multiple posts
#   test = models.Post.limit(2).get()

#   for post in test:
#     post.user()
#     post.tag()

def check_role(roles_list, r):
    for role in roles_list:
        if role['name'] == r.name:
            return True

    return False


def sort_post_by_date(posts):
    sorted_posts = sorted(posts,
                          key=lambda post: datetime.strptime(
                              str(post['published_at']),
                              "%Y-%m-%d %H:%M"),
                          reverse=True)
    return sorted_posts

@app.route('/posts/<int:post_id>')
def get_post(post_id):
    post = models.Post.find(post_id)
    if post is None:
        abort(404, 'Post not found.')

    response = jsonify(Post=post.to_dict())
    response.status_code = 200
    return response

@app.route('/posts/<int:post_id>', methods=['DELETE'])
def delete_post(post_id):
    post = models.Post.find(post_id)
    if post is None:
        abort(404, 'Post not found.')

    if ('username' in session) and (session['username'] == post.user.username):
        if post.delete() is True:
            response = jsonify({'message': 'Post deleted successfully',
                                'status_code': 200})
            response.status_code = 200
            return response


@app.route('/posts/<int:post_id>', methods=['PUT'])
def update_post(post_id):
    expected_key_list = ['username',
                         'title',
                         'tag',
                         'content',
                         'published_at']

    request_post = escape_post(request.get_json(force=True))

    if sorted(expected_key_list) != sorted(request_post.keys()) or validate_date(request_post['published_at']) is False:
        abort(400, 'Invalid request')

    post = models.Post.find(post_id)
    if post is None:
        abort(404, 'Post not found.')



    if ('username' in session) and (session['username'] == post.user.username):
        if post.update(title=request_post['title'],
                       content=request_post['content'],
                       published_at=request_post['published_at']):
            response = jsonify({'message': 'Post updated successfully',
                                'status_code': 200})
            response.status_code = 200
            return response


@app.route('/posts/user/<string:username>/')
def get_user_posts(username):
    username = escape(username)

    user = models.User.where('username', username).first()
    if user is None:
        abort(404, 'User not found')

    posts = user.posts().get()
    if posts is None:
        abort(404, 'User has 0 post.')

    for post in posts:
        post.user()
        post.tag()

    sorted_posts = sort_post_by_date(posts.to_dict())

    return jsonify(Posts=sorted_posts)


@app.route('/posts/user/<string:username>/published')
def get_user_published(username):
    username = escape(username)

    user = models.User.where('username', username).first()
    if user is None:
        abort(404, 'User not found')

    posts = user.posts().published().get()
    if posts is None:
        abort(404, 'User has 0 post.')

    for post in posts:
        post.user()
        post.tag()

    sorted_posts = sort_post_by_date(posts.to_dict())

    return jsonify(Posts=sorted_posts)


@app.route('/posts/user/<string:username>/unpublished')
def get_user_unpublished(username):
    username = escape(username)

    user = models.User.where('username', username).first()
    if user is None:
        abort(404, 'User not found')

    posts = user.posts().unpublished().get()
    if posts is None:
        abort(404, 'User has 0 post.')

    for post in posts:
        post.user()
        post.tag()

    sorted_posts = sort_post_by_date(posts.to_dict())

    return jsonify(Posts=sorted_posts)


@app.route('/posts/tag/<string:tagname>/')
def get_tag_posts(tagname):
    tagname = escape(tagname)

    tag = models.Tag.where('name', tagname).first()
    if tag is None:
        abort(404, 'tag not found')

    posts = tag.posts().get()
    if posts is None:
        abort(404, 'User has 0 post.')

    for post in posts:
        post.user()
        post.tag()

    sorted_posts = sort_post_by_date(posts.to_dict())

    return jsonify(Posts=sorted_posts)


@app.route('/posts/tag/<string:tagname>/published')
def get_tag_published(tagname):
    tagname = escape(tagname)
    tag = models.Tag.where('name', tagname).first()

    if tag is None:
        abort(404, 'tag not found')

    posts = tag.posts().published().get()

    if posts is None:
        abort(404, 'User has 0 post.')

    for post in posts:
        post.user()
        post.tag()

    sorted_posts = sort_post_by_date(posts.to_dict())

    return jsonify(Posts=sorted_posts)


@app.route('/posts/tag/<string:tagname>/unpublished')
def get_tag_unpublished(tagname):
    tagname = escape(tagname)
    tag = models.Tag.where('name', tagname).first()

    if tag is None:
        abort(404, 'tag not found')

    posts = tag.posts().unpublished().get()

    if posts is None:
        abort(404, 'User has 0 post.')

    for post in posts:
        post.user()
        post.tag()

    sorted_posts = sort_post_by_date(posts.to_dict())

    return jsonify(Posts=sorted_posts)





@app.route('/login', methods=['GET', 'POST'])
def login():
    expected_key_list = ['username',
                         'password']

    credentials = request.get_json(force=True)

    if sorted(expected_key_list) != sorted(credentials.keys()):
        abort(400, 'Invalid request.')

    if ('username' in session) and (session['username'] == credentials['username']):
        abort(409, 'Already logged in.')

    user = validate(credentials['username'], credentials['password'])

    if user is None:
        abort(401, 'Login failed')

    user.roles()  # Load roles
    session['username'] = escape(credentials['username'])
    session['password'] = escape(credentials['password'])
    session['user_id'] = user.id
    session['roles'] = user.roles.to_dict()  # session['roles'][0]['name']

    session.permanent = False  # the session will be deleted when the user closes the browser.

    # data = {"username": str(escape(session['username'])), "user_id": user.id,
    # "password": str(escape(session['password']))}



    return jsonify(Profile=user.to_dict())


def validate(u, p):
    user = models.User.where('username', u).where('password', p).first()
    return user


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    # remove the username from the session if it's there
    # session.pop('username', None)
    session.clear()  # clear session dict
    return redirect(url_for('index'))


@app.route('/test', )
def test():
    # return jsonify(Posts=models.Post.unpublished().get().to_dict())
    return jsonify(Posts=models.Post.has_tag('AI').get().to_dict())

# set the secret key.  keep this really secret:
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

if __name__ == '__main__':
    app.run(debug=True)